import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Scanner;

public class MenuProducto {

    static Scanner s = new Scanner(System.in);
    static Producto p;
    static ArrayList < Producto > listadoProducto = new ArrayList<>();

    private static void crearProducto() {
        int codigo;
        int precio;
        String nombre;

        System.out.println("Ingrese el código del producto");
        codigo = s.nextInt();
        System.out.println("Ingrese el nombre del producto");
        nombre = s.next();
        System.out.println("ingrese el precio del producto");
        precio = s.nextInt();

        p = new Producto(codigo, nombre, precio);
        listadoProducto.add(p);
    }

    private static void listarProducto() {
        for(Producto auxiliar : listadoProducto){
            System.out.println("Codigo: "+ auxiliar.getCodigo() + " Nombre: " + auxiliar.getNombre() + " Precio: $" + auxiliar.getPrecio() );
        }
    }

    private static void buscarProducto() {
        int codigoBusqueda;
        boolean encontrado = false;

        System.out.println("Ingrese el código de búsqueda");
        codigoBusqueda = s.nextInt();

        for ( Producto aux : listadoProducto ) {
            if ( aux.getCodigo() == codigoBusqueda ) {
                System.out.println("Codigo: "+ aux.getCodigo() + " Nombre: " + aux.getNombre() + " Precio: $" + aux.getPrecio() );
                encontrado = true;
            }
        }

        if(!encontrado)
            System.out.println("No existe ese código de producto");
    }

    private static void actualizarProducto() {

        int codigo, nuevoPrecio, j, i = -1;
        String nuevoNombre;

        System.out.println("Ingrese el codigo de producto a actualizar");
        codigo = s.nextInt();
        System.out.println("Ingrese el nuevo precio");
        nuevoPrecio = s.nextInt();
        System.out.println("Ingrese el nuevo nombre");
        nuevoNombre = s.next();

        j=0;
        for ( Producto aux : listadoProducto ) {
            if(aux.getCodigo()==codigo){
                i = j;
            }
            j++;
        }

        if( i != -1 ){
            listadoProducto.get(i).setNombre(nuevoNombre);
            listadoProducto.get(i).setPrecio(nuevoPrecio);
        }else{
            System.out.println("No se puede actualizar porque NO existe un producto con ese código");
        }
    }

    private static void eliminarProducto() {

        int codigoBusqueda;
        int i = -1;
        int j;

        System.out.println("Ingrese el código a eliminar");
        codigoBusqueda = s.nextInt();

        j = 0;
        for ( Producto aux : listadoProducto){

            if ( aux.getCodigo() == codigoBusqueda ){
                i = j;
            }
            j++;
        }
        if ( i != 1 ){
            listadoProducto.remove(i);
        }else{
            System.out.println("No existe el código del producto a eliminar");
        }
    }

    private static boolean Menu() {
        boolean repetir = true;
        int opcion;

        System.out.println("Menu");
        System.out.println("1. Agregar un nuevo producto");
        System.out.println("2. Buscar producto por codigo");
        System.out.println("3. Listar todos los productos");
        System.out.println("4. Actualizar producto");
        System.out.println("5. Eliminar producto");
        System.out.println("6. Salir");
        opcion = s.nextInt();

        switch(opcion){
            case 1: crearProducto();
                break;
            case 2: buscarProducto();
                break;
            case 3: listarProducto();
                break;
            case 4: actualizarProducto();
                break;
            case 5: eliminarProducto();
                break;
            case 6: repetir=false;
                break;
        }
        return repetir;
    }

    public static void main(String[] args) {
        while(Menu());
    }

}
